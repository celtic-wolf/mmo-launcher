# mmo-launcher

> An MMO launcher made with electron

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

```

This project was build with [electron](https://electronjs.org/)
For a detailed explanation on how things work, check out the [guide](https://electronjs.org/docs/tutorial/first-app) and [quick start for electron](https://electronjs.org/docs/tutorial/quick-start).
